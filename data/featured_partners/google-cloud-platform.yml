title: GitLab on Google Cloud 
canonical_path: /partners/technology-partners/google-cloud-platform/
seo_title: GitLab on Google Cloud 
description: Unite teams and workflows with GitLab, a complete DevOps platform to build, test, and deploy on Google Cloud.  
title_description: Unite teams and workflows with GitLab, a complete DevOps platform to build, test, and deploy on Google Cloud.  
logo: /images/logos/google_cloud.svg
logo_alt: Google Cloud Logo
body_top_title: One interface for every use case
body_top_description: >-
  As an open DevOps platform, GitLab lowers the barrier of entry for cloud native solutions like Kubernetes, Knative, and Istio. Simplify your multicloud, hybrid cloud, or all-in-the-cloud deployment strategy with one solution for everyone on your pipeline. 

  
  Receive a $300 sign-up credit for every new GCP account associated with your business email. Plus, new and existing accounts can redeem an additional $200 partner credit to get started with GitLab on GCP.

body_top_cta_1_text: Read the whitepaper
body_top_cta_2_text: Get $200 in partner credit
body_top_cta_img_1: "/images/topics/featured-partner-gcp-1.svg"
body_top_cta_img_2: "/images/topics/featured-partner-gcp-2.svg"
body_top_cta_1_url: https://cloud.google.com/solutions/modern-cicd-anthos-user-guide
body_top_cta_2_url: https://cloud.google.com/partners/partnercredit?pcn_code=0014M00001h35gDQAQ%3Futm_campaign%3D2018_cpanel&utm_source=gitlab&utm_medium=referral
body_features_title: Develop better cloud native applications faster with GitLab and GCP
body_features_description: >-
  GitLab’s tight integrations with Google Cloud services enable workflows for every workload. Shrink cycle times by driving efficiency at every stage of the software development process. 
  

  From idea to production on Google Cloud, GitLab’s complete DevOps platform delivers built-in planning, monitoring, and reporting solutions for modern applications.
body_quote: We had developers that thought, Why would we do something else? Jenkins is fine. But I think those people need to see GitLab first and see what the difference is because GitLab is so much more than Jenkins. The power of GitLab is you can do so much more and you can make everything so much easier to manage.
body_quote_source: Michiel Crefcoeur, Frontend Build and Release Engineer at ANWB
feature_items:
  - title: "Collaborate practically"
    description: "Iterate faster, transform together. Modern CI/CD with Anthos reduces rework so happier developers and cloud practitioners can deliver product roadmaps instead of repairing old roads."
  - title: "Automate securely "
    description: "Lock up your process. Automated DevSecOps workflows increase uptime by reducing security and compliance risks on Google Cloud infrastructure."
  - title: "Celebrate repeatedly"
    description: "Deliver when it matters, where it matters. Increase market share and revenue when your product is on budget, on time, and always up on GCP."
body_middle_title: "Get started with GitLab and GCP joint solutions"
body_middle_items:
  - title: "Google Kubernetes Engine (GKE)"
    description: "GKE is Google’s managed Kubernetes service, designed to automate deployment, scaling, and management of containerized Linux and Windows applications. With GitLab’s GKE integration, teams can quickly provision new GKE clusters or import existing clusters in just a few clicks. Leverage GitLab’s Auto DevOps functionality for the lowest barrier of entry to deploy container workloads to GKE with CI/CD."
    icon: "/images/logos/google-kubernetes-engine.png"
    link: "/blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/"
  - title: "Anthos"
    description: "Anthos is a modern application platform that provides a consistent development and operations experience for on-premise and cloud environments. GitLab supports GKE On Premise (GKE-OP), CloudRun for Anthos, and Anthos Configuration Management for workflow optimization on top of Anthos' unified infrastructure management platform. Plus, GitLab supports on-prem GKE for hybrid cloud customers. Together, GitLab with Anthos provides enterprises with consistency and scalability across heterogeneous environments."
    icon: "/images/logos/anthos.png"
    link: "https://cloud.google.com/solutions/partners/deploying-gitlab-anthos-gke-on-prem-cicd-pipeline"
  - title: "Cloud Run"
    description: "Cloud Run is a fully managed serverless platform that automatically scales stateless containers and abstracts away all infrastructure management. Deploy to Cloud Run with GitLab Serverless – a full CI/CD workflow to build and test serverless applications. With GitLab for Cloud Run, teams can streamline and simplify serverless management on any infrastructure (Knative, Cloud Run, Cloud Run for Anthos, etc.) through a single UI."
    icon: "/images/logos/google-cloud-run.png"
    link: "https://docs.gitlab.com/ee/user/project/clusters/serverless/"
  - title: "Google Compute Engine"
    description: "Google Compute Engine (GCE) delivers configurable, high-performance virtual machines running in Google’s data centers. GitLab CI/CD provides application delivery to virtual machines as deployment targets. Migrate traditional, non-containerized workloads to the cloud with GitLab. Get started by installing GitLab on a single GCE instance or in High Availability architecture."
    icon: "/images/logos/google-compute-engine.png"
    link: "https://docs.gitlab.com/ee/install/google_cloud_platform/"
  - title: "Google App Engine"
    description: "Google App Engine (GAE) is a Platform as a Service for hosting serverless and web applications in Google-managed data centers. With GitLab, teams can collaborate and automatically deploy containers directly onto GAE via CI. As a single DevOps application, GitLab enables teams to build and deploy code to yet another serverless type platform."
    icon: "/images/logos/google-app-engine.png"
    link: "https://medium.com/faun/deploy-directly-from-gitlab-to-google-app-engine-d78bc3f9c983"
  - title: "Google Cloud Functions"
    description: "Google Cloud Functions (GCF) is Google Cloud’s event-driven serverless compute platform. Store your code in GitLab SCM and directly deploy as cloud functions through GitLab CI/CD. Empower your teams to adopt GCP for a more event-driven, cloud native architecture with GitLab and GCF by, for example, automating development for Firebase and Cloud Functions."
    icon: "/images/logos/goolge-cloud-functions.png"
    link: "https://cloud.google.com/functions"
  - title: "Firebase"
    description: "Firebase is a platform for creating mobile and web applications developed by Google. Together, GitLab SCM and CI help developers automate with first-class CI/CD pipelines to build, test, and deploy updates frequently to the entire Firebase stack."
    icon: "/images/logos/google_firebase_image.png"
    link: "/blog/2020/03/16/gitlab-ci-cd-with-firebase/"
benefits_title: Discover the benefits of GitLab on GCP
featured_video_title: "Opening Keynote: The Power of GitLab - Sid Sijbrandij"
featured_video_url: "https://www.youtube-nocookie.com/embed/xn_WP4K9dl8"
featured_video_topic: "GitLab Commit Virtual 2020"
featured_video_more_url: "https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/featured"
resources:
  - title: Kubernetes and the future of cloud native - We chat with Kelsey Hightower
    url: /blog/2019/05/13/kubernetes-chat-with-kelsey-hightower/
    type: Blog
  - title: How to leverage GitLab CI/CD for Google Firebase
    url: /blog/2020/03/16/gitlab-ci-cd-with-firebase/
    type: Blog
  - title: CloudRun for Anthos
    url: /blog/2019/11/19/gitlab-serverless-with-cloudrun-for-anthos/
    type: Blog
  - title: GitLab Google Cloud Platform Solution Brief
    url: /resources/downloads/GitLab_GCP_Solution_Brief.pdf
    type: whitepaper
cta_banner:
  - title: Explore GitLab on Google Cloud now
    button_url: /sales/
    button_text: Start your free trial       
