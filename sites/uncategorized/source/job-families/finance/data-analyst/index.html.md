---
layout: job_family_page
title: "Data Analytics"
---

[Data Analytics Handbook Page](/handbook/business-ops/data-team/organization/analytics/)

## Levels

### Intern

An intern is not required to meet the standards of an intermediate data analyst but she or he is required to be interested in developing in towards them.
An intern must:
* Have a track record of asking hard questions and thinking critically
* Self-starter committed to remote work and its intricacies
* Proactive, positive, energetic, customer service personality
* Ability to articulate in a clear, concise manner, disseminating complete and accurate information
* Ability to deal effectively with people of multi-cultural societies
* Attention to detail
* Organizational skills

#### What you'll do
* Work with and learn from a talented team of data professionals
* Develop and execute a project with the help of a mentor
* Write blog posts about your learnings
* Update, maintain, and coordinate meetings
* Update the handbook using git and GitLab
* Identify Data team process weaknesses and blindspots
* Contribute fresh perspective and speak up where you can add value

### Junior Data Analyst

The Junior Data Analyst reports to the department manager or director.

#### Junior Data Analyst Job Grade

The Junior Data Analyst is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Junior Data Analyst Responsibilities

* Collaborate with other functions across the company by building reports and dashboards with useful analysis and data insights
* Explain trends across data sources, potential opportunities for growth or improvement, and data caveats for descriptive, diagnostic, predictive (including forecasting), and prescriptive data analysis
* Deep understanding of how data is created and transformed through GitLab products and services provided by [third-parties](/handbook/business-ops/data-team/#-extract-and-load) to help drive product designs or service usage or note impacts to data reporting capabilities
* Understand and document the full lifecycle of data and our common data framework so that all data can be integrated, modeled for easy analysis, and analyzed for data insights
* Document every action in either issue/MR templates, the [handbook](/handbook/), or READMEs so your learnings turn into repeatable actions and then into automation following the GitLab tradition of [handbook first!](/handbook/handbook-usage/#why-handbook-first)
* Expand our database with clean data (ready for analysis) by implementing data quality tests while continuously reviewing, optimizing, and refactoring existing data models
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale database environment. Maintain and advocate for these standards through code review
* Contribute to and implement data warehouse and data modeling best practices, keeping reliability, performance, scalability, security, automation, and version control in mind
* Follow and improve our processes and workflows for maintaining high quality data and reporting while implementing the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do

#### Junior Data Analyst Requirements

* 1+ years experience in an analytics role
* Experience building reports and dashboards in a data visualization tool
* Passionate about data, analytics and automation. Experience cleaning and modeling large quantities of raw, disorganized data (we use dbt)
* Experience with a variety of data sources. Our data includes Salesforce, Zuora, Zendesk, Marketo, NetSuite, Snowplow and many others (see the [data team page](/handbook/business-ops/data-team/#-extract-and-load))
* Demonstrate capacity to clearly and concisely communicate complex business logic, technical requirements, and design recommendations through iterative solutions
* Deep understanding of SQL in analytical data warehouses (we use Snowflake SQL) and in business intelligence tools (we use Periscope)
* Hands on experience working with SQL, Python, API calls, and JSON, to generate business insights and drive better organizational decision making
* Familiarity with Git and the command line
* Deep understanding of relational and non-relational databases, SQL and query optimization techniques, and demonstrated ability to both diagnose and prevent performance problems
* Effective communication and [collaboration](/handbook/values/#collaboration) skills, including clear status updates
* Positive and solution-oriented mindset
* Comfort working in a highly agile, [intensely iterative](/handbook/values/#iteration) environment
* [Self-motivated and self-managing](/handbook/values/#efficiency), with strong organizational skills
* Ability to thrive in a fully remote organization
* Share and work in accordance with our values
* Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks)
* Ability to use GitLab.

### Data Analyst (Intermediate)

The Data Analyst reports to the department manager or director.

#### Data Analyst (Intermediate) Job Grade

The Data Analyst (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Data Analyst (Intermediate) Responsibilities

* Extends that of the Junior Data Analyst responsibilities

#### Data Analyst (Intermediate) Requirements

* Extends that of the Junior Data Analyst requirements
* 2+ years experience in an analytics role

### Senior Data Analyst

The Senior Data Analyst reports to the department manager or director.

#### Senior Data Analyst Job Grade

The Senior Data Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Data Analyst Responsibilities

* Extends that of the Data Analyst (Intermediate) responsibilities

#### Senior Data Analyst Requirements  

* Extends that of the Data Analyst (Intermediate) requirements
* Advocate for improvements to data quality, security, and query performance that have particular impact across your team as a Subject Matter Expert (SME)
* Solve technical problems of high scope and complexity
* Exert influence on the long-range goals of your team
* Understand the code base extremely well in order to conduct new data innovation and to spot inconsistencies and edge cases
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment; Maintain and advocate for these standards through code review
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions
* Provide mentorship for Junior and Intermediate Engineers on your team to help them grow in their technical responsibilities
* Deliver and explain data analytics methodologies and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Build close relationships with other functional teams to truly democratize data understanding and access
* Influence and implement our service level framework [SLOs](/handbook/business-ops/data-team/platform/#slos-service-level-objectives-by-data-source) and SLAs for our data sources and data services
* Identifies changes for the product architecture and from third-party services from the reliability, performance and availability perspective with a data driven approach focused on relational databases, knowledge of another data storages is a plus
* Proactively work on the efficiency and capacity planning to set clear requirements and reduce the system resources usage to make compute queries cheaper
* Participate in [Data Quality Process](/handbook/business-technology/data-team/data-quality/) or other data auditing activities

## Specialties

### Data Team
* Provide data modeling expertise to all GitLab teams through code reviews, pairing, and training to help deliver optimal, DRY, and scalable database designs and queries in Snowflake and in Periscope
* Approve data model changes as a Data Team [Reviewer](/handbook/business-technology/data-team/how-we-work/mr-review/#reviewer) and [code owner](https://gitlab.com/gitlab-data/analytics/blob/master/CODEOWNERS) for specific database and data model schemas
* Own the end-to-end process of on-call data triaging from reading Airflow logs, to diagnosing the data issue, and to verifying and implementing a solution with an automated alerting system (ChatOps, etc) as well as providing data support for all GitLab members
* This position reports to the Manager, Data

### Engineering
* Support all departments in the engineering division by helping drive the standardization, capture, automation, and implementation of performance indicators
* Be intimately familiar with productivity metrics
* Priorities will be set by the VP, Engineering but will collaborate with and reporting into the Data Team

### Finance
* Support the FP&A team in driving financial and operational initiatives by analyzing data and discovering insights
* Focus on financial and operational specific data
* Priorities will be set by the Manager, Financial Planning and Analysis but will collaborate with and report into the Data Team
* Spend 80% of time supporting the FP&A team and spend the remaining 20% of time contributing to the Data Team
* The Manager, Financial Planning and Analysis will evaluate the analyst on 80% of the goals in the experience factor worksheet relating to supporting FP&A and the Manager, Data will evaluate the analyst on the remaining 20% of goals relating to supporting the Data Team

### Growth
* Support the product management function in driving product growth, reducing churn, increasing user engagement by analyzing data and discovering insights
* Focus on product-specific data - usage ping, SaaS DB, Snowplow events
* Priorities will be set by a Product Manager, Growth but will collaborate with and report into the Data Team

### Product
* Support the Product function by spearheading tracking and reporting initiatives
* Focus on product usage metrics across SaaS and self-managed products
* Build cross-functional analysis to drive strategic decision-making
* Priorities will be set by a Director of Product but will collaborate with and report into the Data Team

### Sales
* Coordinate with SalesOps to improve and automate tracking potentially insightful data points
* Focus on cross-functional analysis that can help drive sales conversations (e.g. product usage into renewal conversations)
* Priorities will be set by the sales function but will collaborate with and report into the Data Team

### Marketing
#### Analyst
* Coordinate with Marketing to improve and automate tracking potentially insightful data points, by analyzing data and discovering insights
* Support Marketing by helping drive the standardization, capture, automation, and implementation of performance indicators
* Assist with data driven planning and strategy
* Focus on cross-functional analysis that can help drive marketing conversations
* Reports to [Director of Marketing Strategy and Performance](/job-families/marketing/marketing-strategy/#director-marketing-strategy) working closely with the Chief Marketing Officer
* Member of the [Marketing Strategy and Performance Team](/handbook/marketing/strategy-performance/)

#### Web Analyst
* Provide recommendations for technical SEO improvements to the site.
* Implement our SEO strategy together with our marketing team and digital marketing agency.
* Grow traffic to about.gitlab.com through organic search marketing programs.
* Report on marketing site growth from SEO and paid search.
* Troubleshoot and formulate solutions to issues with SEO and site lead flow.
* Evaluate and report data across multiple channels to monitor marketing site growth.
_ Additional Requirements_
* 3+ years in Search Engine Marketing role
* 1-3 years of enterprise software marketing experience.
* 3+ years experience Google Analytics (or related tool), Google Search Console and experience with at least one SEO tool.
* Technical/industry experience focused on SEO online advertising to improve lead generation, sales pipeline, and revenue.
* Reports to [Director of Marketing Strategy and Performance](/job-families/marketing/marketing-strategy/#director-marketing-strategy) working closely with the Chief Marketing Officer
* Member of the [Marketing Strategy and Performance Team](/handbook/marketing/strategy-performance/)


### People
* Coordinate and support the People function by automating all reports from Greenhouse, BambooHR, and Google Sheets into reporting dashboards.
* Focus on cross-functional analysis to help other departments identify opportunities for improvement within their recruiting, hiring, and retention policies.
* Priorities will be set by Director, People Operations but will collaborate with and report into the Data Team

## Performance Indicators (PI)

*  [Adoption of Data Team BI charts throughout company](/handbook/business-ops/metrics/#adoption-of-data-team-bi-charts-throughout-company)
*  [% of issues requested triaged with first response within 36 hours (per business unit)](/handbook/business-ops/metrics/#percent--of-issues-requested-triaged-with-first-response-within-36-hours-per-business-unit)

### Career Ladder

The next step in the Data Analyst job family is to move to the [Data Management](/job-families/finance/manager-data/) job family.

## Hiring Process

* Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

* Selected candidates will be invited to fill out a short questionnaire.
* Next, candidates will be invited to schedule a screening call with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Senior Director, Data and Analytics
* Next, candidates will be invited to schedule a second interview with the business division DRI
* Next, candidates will be invited to schedule a third interview with one a member from our Data team
* Next, candidates will be invited to schedule a fourth interview with a specialty Engineering manager


Additional details about our process can be found on our [hiring page](/handbook/hiring/).
