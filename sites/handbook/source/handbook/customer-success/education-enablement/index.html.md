---
layout: handbook-page-toc
title: "Customer Success Education and Enablement"
description: "As a Solutions Architect, Technical Account Manager, or Professional Services Engineer, it is important to be continuously learning more about our product and related industry topics. This handbook page provides an dashboard of aggregated resources that we encourage you to use to get up to speed."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview of Education and Enablement

As a Solutions Architect, Technical Account Manager, or Professional Services Engineer, it is important to be continuously learning more about our product and related industry topics. This handbook page provides an dashboard of aggregated resources that we encourage you to use to get up to speed. Although we aggregated and/or developed these resources for Customer Success team members, they are generic enough that all team members and partners can benefit from the education and enablement resources that we have published here.

## Educational Tiers

There is a lot of depth to the GitLab product and the industry knowledge that you'll need to know (eventually). To help you level up, the resources on this page have been broken up into educational tiers.

* **Foundations (101 Level)** - This provides an overview that lets you get a summary about a topic in less than 30 minutes to be able to have a high-level conversation about it.
* **Associate (150 to 200 Level)** - This provides a working knowledge of the depth of a topic and associated features. This provides enough information to know that a feature exists and a "good enough" understanding of how it works until you find yourself needing to focus on a topic for a more complex demo or implementation.
* **Professional (300-400 Level)** - This provides the subject matter expert level materials for topics of interest. If you become proficient in a topic, you are encouraged to contribute resources (videos, demos, post-mortem write-ups, documentation pages, repositories, etc.) to this tier since this is crowd sourced from team members and not formally developed as part of our education and enablement program.

## Overview of GitLab Feature and Use Cases

The GitLab product is organized based on the DevOps stages.

### DevOps Stages

If you're new to GitLab and the concept of "stages" are new to you, simply think of it as a category of related features that a customer finds value in with a **technical** narrative. The stages are organized in an infinite loop diagram to show the software development lifecycle from planning to development to release, with additional value for deployed applications with security and monitoring features. You can see a high-level overview of each stage on the [marketing page](https://about.gitlab.com/stages-devops-lifecycle/). Be sure to click the `Learn More` link for each stage to see more about the features of that stage.

One of the most helpful resources for understanding what GitLab does, the features in each stage, and how well the feature works is our [Category maturity](https://about.gitlab.com/direction/maturity/) chart. This may also be informally referred to as our "lovable feature chart".

If you understand what the stages are and want to dive deeper, you can look at the [product categories handbook page](https://about.gitlab.com/handbook/product/categories/) for a deep-dive on each of the stages, specifically the [hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy) and the [DevOps Stages](https://about.gitlab.com/handbook/product/categories/#devops-stages).

For the purposes of education and enablement, we focus on the DevOps stages that the customer sees. There are additional stages that help GitLab as a business succeed, such as the [Growth stage](https://about.gitlab.com/handbook/product/categories/#growth-stage), [Fulfillment stage](https://about.gitlab.com/handbook/product/categories/#fulfillment-stage), [Enablement stage](https://about.gitlab.com/handbook/product/categories/#enablement-stage), and [Single Engineer Groups stage](https://about.gitlab.com/handbook/product/categories/#single-engineer-groups-section). Although these are not necessarily DevOps stages, the Engineering and Product departments use the stage terminology for consistency. **You do not need to spend time learning about these stages and can search for this information later if the need arises.**

### Use Cases

The GitLab use cases help define the categories of related features with a **business value** narrative. In other words, why are customers buying GitLab and what do they think that we offer. You can read more about each of our use cases by navigating to the Product navigation menu at the top of this page or from anywhere on [about.gitlab.com](https://about.gitlab.com/).

* [Source Code Management](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/)
* [Continuous Integration and Deployment/Delivery (CI/CD)](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
* [Auto DevOps](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/)
* [Security and DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops/)
* [Agile Delivery](https://about.gitlab.com/solutions/agile-delivery/)
* [Value Stream Management](https://about.gitlab.com/solutions/value-stream-management/)
* [GitOps and Infrastructure-as-Code](https://about.gitlab.com/solutions/gitops/)

There are also resources available to cater to the size of customer and their needs.

* [Small Business](https://about.gitlab.com/small-business/)
* [Enterprise](https://about.gitlab.com/enterprise/)

## GitLab Product Topics

To align our education and enablement with our product, each of the topics below are grouped based on how GitLab product management and engineering defines it.

### DevOps Sections
***
##### Foundations Level
- [Dev Section Direction and Roadmap](https://about.gitlab.com/direction/dev/)
- [Ops Section Direction and Roadmap](https://about.gitlab.com/direction/ops/)
- [Security Section Direction and Roadmap](https://about.gitlab.com/direction/security/)

### Manage Stage
***
##### Foundations Level
- [Manage Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/manage/)
- [Manage Stage Direction and Roadmap](https://about.gitlab.com/direction/dev/#manage)
##### Associate Level
- [Value Stream Management Feature Docs](https://about.gitlab.com/solutions/value-stream-management/)
- [Audit Events Feature Docs](https://docs.gitlab.com/ee/administration/audit_events.html)
- [DevOps Reports Feature Docs](https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html)
- [Code Analytics Feature Docs](https://docs.gitlab.com/ee/user/analytics/code_review_analytics.html)
- [Compliance Management Feature Docs](https://docs.gitlab.com/ee/administration/compliance.html)
- [Audit Reports Feature Docs](https://docs.gitlab.com/ee/administration/audit_reports.html)

### Plan Stage
***
##### Foundations Level
- [Plan Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/plan/)
- [Plan Stage Direction and Roadmap](https://about.gitlab.com/direction/dev/#plan)
##### Associate Level
- [Issue Tracking Feature Docs](https://docs.gitlab.com/ee/user/project/issues/)
- [Time Tracking Feature Docs](https://about.gitlab.com/solutions/time-tracking/)
- [Boards Feature Docs](https://about.gitlab.com/stages-devops-lifecycle/issueboard/)
- [Epics Feature Docs](https://about.gitlab.com/product/epics/)
- [Roadmaps Feature Docs](https://about.gitlab.com/stages-devops-lifecycle/roadmaps/)
- [Service Desk Feature Docs](https://about.gitlab.com/stages-devops-lifecycle/service-desk/)
- [Requirements Management Feature Docs](https://docs.gitlab.com/ee/user/project/requirements/)
- [Quality Management Feature Docs](https://docs.gitlab.com/ee/ci/test_cases/index.html)
- [Design Management Feature Docs](https://docs.gitlab.com/ee/user/project/issues/design_management.html)

### Create Stage
***
##### Foundations Level
- [Create Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/create/")
- [Create Stage Direction and Roadmap](https://about.gitlab.com/direction/dev/#create)
##### Associate Level
- [Source Code Management Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/)
- [Source Code Management Direction and Roadmap](https://about.gitlab.com/direction/create/source_code_management/)
- [Code Review Feature Overview with 30+ Features](https://about.gitlab.com/stages-devops-lifecycle/code-review/)
  - [JIRA Integration Solution Overview](https://about.gitlab.com/solutions/jira/)
  - [JIRA Issues Integration Feature Docs](https://docs.gitlab.com/ee/integration/jira/)
  - [JIRA Development Panel Integration Feature Docs](https://docs.gitlab.com/ee/integration/jira/dvcs.html)
  - [Multiple approvers in code review Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)
  - [Approval rules for code review Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)
  - [Merge request dependencies Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_dependencies.html)
  - [Code Owners Feature Docs](https://docs.gitlab.com/ee/user/project/code_owners.html)
- [Wiki Feature Docs](https://docs.gitlab.com/ee/user/project/wiki/)
- [Static Site Editor Feature Docs](https://docs.gitlab.com/ee/user/project/static_site_editor/)
- [Web IDE Feature Docs](https://docs.gitlab.com/ee/user/project/web_ide/index.html)
- [Live Preview Feature Docs](https://docs.gitlab.com/ee/user/project/web_ide/index.html#live-preview)
- [Snippets Feature Docs](https://docs.gitlab.com/ee/user/snippets.html)
- [Gitaly Source Code Project](https://gitlab.com/gitlab-org/gitaly)

### Verify Stage
***
##### Foundations Level
- [Verify Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/verify/)
- [Verify Stage Direction and Roadmap](https://about.gitlab.com/direction/ops/#verify)
- [CI YouTube Playlist](https://youtube.com/playlist?list=PL05JrBw4t0Ko-mJZLo2uF3aQQuBfBaSKB)
##### Associate Level
- [Continuous Integration (CI) Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
  - [CI/CD Feature Docs](https://docs.gitlab.com/ee/ci/)
  - [CI/CD Concepts Docs](https://docs.gitlab.com/ee/ci/introduction/)
  - [CI/CD Pipeline Docs](https://docs.gitlab.com/ee/ci/pipelines/)
  - [CI/CD Variables Docs](https://docs.gitlab.com/ee/ci/variables/README.html)
  - [CI/CD Environments and Deployments Docs](https://docs.gitlab.com/ee/ci/environments/)
  - [CI/CD with Runners Docs](https://docs.gitlab.com/ee/ci/runners/README.html)
  - [Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)
  - [CI/CD configuration with `.gitlab-ci.yml` Docs](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
  - [CI/CD authoring YAML reference for `.gitlab-ci.yml` Docs](https://docs.gitlab.com/ee/ci/yaml/README.html)
  - [CI/CD Implementation Examples Docs](https://docs.gitlab.com/ee/ci/examples/README.html)
  - [CI/CD Troubleshooting Docs](https://docs.gitlab.com/ee/ci/troubleshooting.html)
  - [Migrate from CircleCI Docs](https://docs.gitlab.com/ee/ci/migration/circleci.html)
  - [Migrate from Jenkins Docs](https://docs.gitlab.com/ee/ci/migration/jenkins.html)
- [Code Quality Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
- [Code Testing and Coverage Feature Docs](https://docs.gitlab.com/ee/ci/unit_test_reports.html)
- [Load Testing Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html)
- [Browser Performance Testing Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html)
- [Usability Testing Feature Docs](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews-starter)
- [Accessibility Testing Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html)
- [Merge Trains Feature Docs](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/)

### Package Stage
***
##### Foundations Level
- [Package Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/package/)
- [Package Stage Direction and Roadmap](https://about.gitlab.com/direction/package/)
##### Associate Level
- [Package Registry Feature Docs](https://docs.gitlab.com/ee/user/packages/package_registry/)
- [Container Registry Feature Docs](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [Helm Chart Registry Feature Docs](https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts)
- [Dependency Proxy Feature Docs](https://docs.gitlab.com/ee/user/packages/dependency_proxy/)
- [Dependency Firewall Direction and Roadmap](https://about.gitlab.com/direction/package/#dependency-firewall)
- [Release Evidence Feature Docs](https://docs.gitlab.com/ee/user/project/releases/#release-evidence)
- [Git LFS - Feature Docs](https://docs.gitlab.com/ee/topics/git/lfs/index.html)

### Release Stage
***
##### Foundations Level
- [Release Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/release/)
- [Release Stage Direction and Roadmap](https://about.gitlab.com/direction/release)
- [CI/CD eBook](https://about.gitlab.com/resources/ebook-single-app-cicd/)
- [CI/CD YouTube Demo Overview](https://about.gitlab.com/blog/2017/03/13/ci-cd-demo/)
##### Associate Level
- [Continuous Delivery Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
  - [See verify stage for full list of CI/CD resources](#verify-stage)
- [Pages Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/pages/)
  - [Pages Feature Direction and Roadmap](https://about.gitlab.com/direction/release/pages)
  - [Pages Feature Docs](https://docs.gitlab.com/ee/user/project/pages/)
- [Review Apps Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/review-apps/)
  - [YouTube Webcast Feature Overview](https://www.youtube.com/watch?v=CteZol_7pxo&feature=youtu.be)
  - [Review Apps Direction and Roadmap](https://gitlab.com/groups/gitlab-org/-/epics/495)
  - [Review Apps Feature Docs](https://docs.gitlab.com/ee/ci/review_apps)
- [Advanced Deployments Feature Docs](https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production-premium)
- [Feature Flags Feature Docs](https://docs.gitlab.com/ee/operations/feature_flags.html)
- [Release Orchestration Feature Docs](https://docs.gitlab.com/ee/user/project/releases/)

### Configure Stage
***
##### Foundations Level
- [Configure Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/configure/)
- [Configure Stage Direction and Roadmap](https://about.gitlab.com/direction/configure/)
##### Associate Level
- [Auto DevOps Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/)
- [Auto DevOps Feature Docs](https://docs.gitlab.com/ee/topics/autodevops/)
- [Kubernetes Management Feature Overview](https://about.gitlab.com/solutions/kubernetes/)
- [Secrets Management CI Variable Docs](https://docs.gitlab.com/ee/ci/variables/)
- [ChatOps Feature Docs](https://docs.gitlab.com/ee/ci/chatops/)
- [Serverless Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/serverless/)
- [Infrastructure-as-Code Feature Docs](https://docs.gitlab.com/ee/user/infrastructure/)
- [Cluster Cost Management Feature Docs](https://docs.gitlab.com/ee/user/clusters/cost_management.html)

### Monitor Stage
***
##### Foundations Level
- [Monitor Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/monitor/)
- [Monitor Stage Direction and Roadmap](https://about.gitlab.com/direction/monitor/)
##### Associate Level
- [Runbooks Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/runbooks/)
- [Metrics Feature Docs](https://docs.gitlab.com/ee/operations/metrics/)
- [Incident Management Feature Docs](https://docs.gitlab.com/ee/operations/incident_management/)
- [Logging Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html#kubernetes-pod-logs)
- [Tracing Feature Docs](https://docs.gitlab.com/ee/operations/tracing.html)
- [Error Tracking Feature Docs](https://docs.gitlab.com/ee/operations/error_tracking.html)
- [Synthetic Monitoring Direction and Roadmap](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8338)
- [Product Analytics Feature Docs](https://docs.gitlab.com/ee/operations/product_analytics.html)

### Secure Stage
***
##### Foundations Level
- [Secure Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/secure/)
- [Secure Stage Direction and Roadmap](https://about.gitlab.com/direction/secure/)
##### Associate Level
- [Static Application Security Testing (SAST) Feature Docs](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Dynamic Application Security Testing (DAST) Feature Docs](https://docs.gitlab.com/ee/user/application_security/dast/)
- [Fuzz Testing Feature Docs](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)
- [Dependency Scanning Feature Docs](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [License Compliance Feature Docs](https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html)
- [Secret Detection Feature Docs](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- [Vulnerability Management Feature Docs](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)

### Protect Stage
***
##### Foundations Level
- [Protect Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/protect/)
- [Protect Stage Direction and Roadmap](https://about.gitlab.com/direction/protect/)
##### Associate Level
- [Container Scanning Feature Docs](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Security Orchestration Feature Doc](https://docs.gitlab.com/ee/user/application_security/threat_monitoring/#configuring-network-policy-alerts)
- [Container Host Security Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/protect/container_host_security/index.html)
- [Container Network Security Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/protect/container_network_security/index.html)


## GitLab Training and Certification Program

### Professional Services Classes

Individual GitLab team members can request to audit a customer-facing training sessions delivered by Professional Services. Internal GitLab team leads can request training sessions delivered by Professional Services for their teams.

To learn more, see the [Working with Professional Services handbook page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/working-with/#requesting-training-for-gitlab-team-members).

### GitLab Technical Certifications

GitLab offers technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers.

To learn more, see the [GitLab Technical Certifications handbook page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/).

## Industry Topics

### Cloud Infrastructure

TODO AWS, GCP, DigitalOcean

### Container Technologies

TODO Openshift, Docker

#### Kubernetes Certification
***
##### Foundations Level
- [Kubernetes for Beginners Udemy Training](https://www.udemy.com/course/learn-kubernetes/)
- [CKA Udemy Training](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/)
- [CKAD Udemy Training](https://www.udemy.com/course/certified-kubernetes-application-developer/)
##### Associate Level
- [Kubernetes Documentation](https://kubernetes.io/docs/home/)
##### Professional Level
- [CKA Certification](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/)
- [CKAD Certification](https://training.linuxfoundation.org/certification/certified-kubernetes-application-developer-ckad)
- [CKS Certification](https://training.linuxfoundation.org/certification/certified-kubernetes-security-specialist/)

### Infrastructure-as-Code

#### HashiCorp Terraform
***
##### Foundations Level
- [Hashicorp Learn Tutorials](https://learn.hashicorp.com/terraform)
- [GitLab Terraform Integration Docs](https://docs.gitlab.com/ee/user/infrastructure/)
##### Associate Level
- [Terraform Docs](https://www.terraform.io/docs/index.html)
- [Terraform AWS Provider Docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
- [Terraform GCP Provider Docs](https://registry.terraform.io/providers/hashicorp/google/latest/docs)
- [Terraform GitLab Provider Docs](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs)
- [GitLab Sandbox Cloud](/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started)
- [GitLab Demo Systems Terraform Modules](https://gitlab.com/gitlab-com/demo-systems/terraform-modules)
##### Professional Level
- [Terraform Associate Certification](https://www.hashicorp.com/certification/terraform-associate)

#### HashiCorrp Vault
***
##### Foundations Level
- [Hashicorp Learn Tutorials](https://learn.hashicorp.com/terraform)
##### Associate Level
- TODO
##### Professional Level
- [Vault Associate Certification](https://learn.hashicorp.com/terraform)

#### HashiCorp Consul
***
##### Foundations Level
- [Hashicorp Learn Tutorials](https://learn.hashicorp.com/consule)
##### Associate Level
- TODO
##### Professional Level
- [Consul Associate Certification](https://www.hashicorp.com/certification/consul-associate)

TODO Packer, Ansible, Chef, Puppet

### Web Application Security

TODO OWASP, Tenable, etc.

## Demo Readiness

Getting started demos for baseline proficiency. Learn more on the [Demo Readiness](/handbook/customer-success/solutions-architects/demonstrations/#demo-readiness) page

## Demo Library

TODO Table of all sample projects and video walk through demos

## Enablement Events and Async Recordings

### Customer Success Skills Exchange

The Field Enablement Team hosts a 50 min weekly webinar focused on topics relevant to the Customer Success Team. To learn more, view the upcoming schedule, or check out a recording go to the [CS Skills Exchange handbook page](/handbook/sales/training/customer-success-skills-exchange/).

### Sales Enablement Level Up

The Field Enablement Team hosts a weekly 30 minute webinar focused on topics relevant to the entire Field Team. To learn more, view the upcoming scheudle, or check out a recording go to the [Sales Level Up Webinar handbook page](/handbook/sales/training/sales-enablement-sessions/).

### Command of Message & MEDDPPICC

GitLab uses ForceManagement's Command of Message and MEDDPPICC as our sales methodology. You can familiarize yourself with the kep components and messaging on [Command of Message handbook page](/handbook/sales/command-of-the-message/).

## Helpful Handbook Pages and Internal Google Drive Links

TODO List of bookmarks

## Additional Resources

- [Field Enablement Handbook Page](/handbook/sales/field-operations/field-enablement/)
- [Sales Training](/handbook/sales/training/)
- [O'Reilly](https://learning.oreilly.com/home/). Contact your manager for a license.
- [Communities of Practice](/handbook/customer-success/initiatives/communities-of-practice)
- [Learn at GitLab](https://about.gitlab.com/learn/)
