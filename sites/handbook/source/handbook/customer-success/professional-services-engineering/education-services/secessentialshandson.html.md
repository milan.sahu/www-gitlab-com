---
layout: handbook-page-toc
title: "GitLab Security Essentials Hands On Guide"
description: "This Hands On Guide is designed to walk you through the lab exercises used in the GitLab Security Essentials course."
---
# GitLab Security Essentials Hands On Guide
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Security Essentials Labs
* [Lab 1- Create A Project and Run Security Scanning](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson1.html)
* [Lab 2- Review and create actions on Vulnerabilities](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson2.html)
* [Lab 3- Add Manual k6 Load testing](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson3.html)
* [Lab 4- Run a Fuzz test](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson4.html)

## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab Security Essentials Course Description](https://about.gitlab.com/services/education/security-essentials/)
* [GitLab Security Specialist Certifcation Details](https://about.gitlab.com/services/education/gitlab-security-specialist/)


### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab Security Essentials- please submit your changes via Merge Request!

