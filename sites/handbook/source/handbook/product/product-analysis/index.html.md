---
layout: markdown_page
title: Product Analysis Group
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Welcome to the Product Analysis group Handbook

The Product Analysis group consists of a team of product analysts. This group reports to the 
Director of Product, Growth and serves as a specialized analytics team to support product data 
analysis requests. The primary customer this group serves is Product Managers from various 
sections/groups.

Read more about what we do at GitLab on our [Direction](/direction/product-analysis/) page.

### Product Analysis group members

* Carolyn Braza: Manager, Product Data Analysis
* Dave Peterson: Senior Product Analyst
* Nicole Galang: Product Analyst
* Product Analyst: TBH

The product analysts also work closely with members from the central Data team, including:
* Mathieu Peychet: Senior Data Analyst

The Product Analysis group hopes to partner more closely with data engineers on the Data 
team, as well.

## Intake & Planning Process

### Issue Submission Process

1. For all product analysis requests, please create an issue on [data team project](https://gitlab.com/gitlab-data/analytics/-/issues), 
and add a `product analysis` label, as well as your section and group labels.
  * If the issue is for an experiment analysis, use the [experiment issue template](https://gitlab.com/gitlab-data/analytics/-/blob/master/.gitlab/issue_templates/experiment_template.md).
1. All data issues with `product analysis` labels will appear on a [product analysis board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20analytics).
1. In the issue, please explain the urgency/criticality of the requests and provide as much context as possible.
1. The Manager, Product Data Analysis will help prioritize the work based on importance/capacity and 
work closely with the Director of Product Growth, Chief Product Officer and VP, Product Management on trade-offs if needed.

### Issue Prioritization Process (To be completed by the Product Analysis team)

1. The product analysis group works on a 2-week milestone cadence
  * You can see our current milestone [here](https://gitlab.com/gitlab-data/analytics/-/boards/2329061?scope=all&label_name[]=product%20analysis&milestone_title=%23started).
1. Prioritization to be set during the planning meeting the week before the start of the new milestone
1. Prioritization labels will be added to reflect the issue's impact and urgency
1. Issue will be assigned an estimated projected time commitment
1. New issues will be considered alongside existing issues to set prioritization for the upcoming 
milestone, adjusting task load based on past milestone performance.

## Capacity

As the Product Analysis team is still fairly new and small, we expect most PMs to self-serve 
on analysis and only create data issues for critical asks. As the group grows, so will our ability 
to support additional asks and analyses.

## Slack

1. [#s_growth_data](https://gitlab.slack.com/messages/s_growth_data/) - For Growth stage-related data questions
1. [#data](https://gitlab.slack.com/messages/data/) - For general data questions 

## Product Analysis Handbook Structure

* [Growth Data Guide](/handbook/product/product-analysis/growth-data-guide/)
* [Experimentation Design & Analysis Framework](/handbook/product/product-analysis/experimentation/)

## Other helpful resources & links

1. [Growth Insights Knowledge Base](/direction/growth/#growth-insights-knowledge-base)
1. [Growth Experiments Knowledge Base](/direction/growth/#growth-experiments-knowledge-base)
1. [Growth Product Handbook](/handbook/product/growth/)
1. [Data for product managers](/handbook/business-technology/data-team/programs/data-for-product-managers/)
1. [Data team How we can help you Page](/handbook/business-technology/data-team/#how-we-can-help-you)
