---
layout: handbook-page-toc
title: "Infrastructure Standards - Realms"
description: "This handbook section defines the latest iteration of infrastructure standards for AWS and GCP across all departments and groups at GitLab."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

This is a list of realms that have customized infrastructure standards based on each realm's business and technical requirements.

### Realms

* [business-tech](/handbook/infrastructure-standards/realms/business-tech)
* [eng-dev](/handbook/infrastructure-standards/realms/eng-dev)
* [eng-infra](/handbook/infrastructure-standards/realms/eng-infra)
* [eng-security](/handbook/infrastructure-standards/realms/eng-security)
* [eng-support](/handbook/infrastructure-standards/realms/eng-support)
* [gitter](/handbook/infrastructure-standards/realms/gitter)
* [saas](/handbook/infrastructure-standards/realms/saas)
* [sales-cs](/handbook/infrastructure-standards/realms/sales-cs)
* [sandbox](/handbook/infrastructure-standards/realms/sandbox)
