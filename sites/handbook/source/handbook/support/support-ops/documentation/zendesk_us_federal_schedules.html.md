---
layout: handbook-page-toc
title: 'Schedules'
category: 'Zendesk US Federal'
description: 'An overview of the Zendesk US Federal schedules'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Schedules in Zendesk are like schedules in most other things: windows of time.
We use these to determine business hours and various regional working hours.

## GitLab Schedules

### [Business Hours](https://gitlab-federal-support.zendesk.com/agent/admin/schedules/360000298411)

* Timezone: Pacific Time (US & Canada)
* Sunday: Closed
* Monday: 0600-1800
* Tuesday: 0600-1800
* Wednesday: 0600-1800
* Thursday: 0600-1800
* Friday: 0600-1800
* Saturday: Closed

## Holidays

All schedules should utilize the same holidays. The ones we currently put into
Zendesk are:

* Easter (this is a "movable feast" holiday, so it changes each year)
* Christmas (December 25th)
* New Years Day (January 1st)

On those day, the schedules "stop", which results in the SLA clock not moving.
This allows for Support to actually have those holidays "off".
